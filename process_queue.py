import os
import sys
from qcg.pilotjob.api.manager import LocalManager
from qcg.pilotjob.api.job import Jobs
from qcg.pilotjob.joblist import JobState
from util import ensure_config_path
from util import each_todo_file


def process_queue():
    """
    Polls job from the todo directory until the queue is empty
    """
    # local
    # manager = LocalManager(server_args=['--system-core',
    #                                    '--nodes', '4'], cfg=None)
    # slurm
    # manager = LocalManager(server_args=['--system-core'], cfg=None)
    manager = LocalManager()

    submitted = {}
    finished = {}

    p = ensure_config_path()

    done_path = os.path.join(p, "success")
    failure_path = os.path.join(p, "failure")

    for filename in each_todo_file():
        jobs = Jobs()
        jobs.load_from_file(filename)
        job_id = manager.submit(jobs)
        submitted[job_id[0]] = filename

    while submitted:
        job_info = manager.info(submitted.keys())
        for k, job in job_info.get('jobs').items():
            state = JobState[job.get('data').get('status')]
            if state.is_finished():
                # job finished move jobfile
                finished[k] = submitted[k]
                del submitted[k]
                print('finished ', job)
                sys.stdout.flush()
                bname = os.path.basename(finished[k])
                if state == JobState.SUCCEED:
                    target_dir = done_path
                else:
                    target_dir = failure_path
                os.rename(finished[k],
                          os.path.join(p, os.path.join(target_dir, bname)))

    manager.finish()


if __name__ == "__main__":
    process_queue()
