#!/bin/bash
#SBATCH --job-name=qcgpilotjob-ex
#SBATCH --nodes=1
#SBATCH --time=00:02:00
#SBATCH --partition=thin


module load 2022
module load QCG-PilotJob/0.13.1-foss-2022a


python3 process_queue.py

