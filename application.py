import sys
import random
import time
import threading
import argparse
from threading import Thread


def task(runtime, th):
    print("htread {}".format(th))
    time.sleep(runtime)


def main(n):
    print(sys.argv)
    runtime = random.randint(3, 10)
    tasks = []
    for i in range(n):
        t = Thread(target=task, args=(runtime, i))
        tasks.append(t)
        t.start()
    for t in tasks:
        t.join()
    # throw random error
    if random.random() < 0.3:
        raise RuntimeError("failed")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nt', default=1, type=int)
    parser.add_argument('--name', default="job", type=str)
    args = parser.parse_args()
    main(args.nt)
