# QCG-Pilot-Job Example

## showcase for multiple short single-threaded jobs

### Requirements

1. The system should be able to execute a list of single-threaded tasks with unpredictable heterogenious runtimes
2. It should be possible to reschedule failed tasks
3. Tasks that were not completed (or not even started) due to time limit should be proceeded in subsequent slurm jobs

If the tasks are short compared to the overall walltime of the SLURM the overhead due to heterogenious runtimes is small.

### Solution

A job prepartion script generates one job file per tasks and safes it under a common directory, by default
`~/.short-job-queue/todo`.

The processing script scans the todo files and uses QCG-Pilot-Job to execute the job accoring to the specification defined in the preparation script.
When a task finishes, the task file is moved to `~/.short-job-queue/success` or `~/.short-job-queue/failure` depending on the final status of the task.

Task files that are to not finalized within the walltime of the SLURM job stay in the todo directory and can be processed in the next round.
The user can decide if failed tasks should be re-run my moving the task files from the failure directory to todo.


### Example session
Load the modules

```
module load 2022
module load QCG-PilotJob/0.13.1-foss-2022a
```

Prepare the run files
```
python generate_job.py
```
The resulting taks files are stored under `~/.short-job-queue/todo`. The default base directory can overriden by setting the enviornment variable SHORT_JOB_PATH

Next, submit the slurm script:
```
sbatch job.sh
```
When the `~/.short-job-queue/todo` is not empty after the SLURM job is finished, submit another instance.




