import csv
from util import add_job_to_queue
from qcg.pilotjob.api.job import Jobs

NUMBER_THREADS=10


with open('gemeenten.2023.csv', newline='',
          encoding='utf-8', errors='ignore') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        job_name = 'gemeente_{}'.format(row['Gemeentecode'])
        job = Jobs().add(name=job_name,
                         exec='python',
                         args=["application.py", "--nt", NUMBER_THREADS, "--name", job_name],
                         stdout='workdir/job.out.' + job_name,
                         stderr='workdir/job.err.' + job_name,
                         model='default',
                         modules=["2022", "Python/3.10.4-GCCcore-11.3.0"],
                         iteration=1,
                         numCores=NUMBER_THREADS)
        add_job_to_queue(job)
