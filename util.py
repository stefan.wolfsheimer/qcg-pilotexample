import os
from pathlib import Path


def ensure_config_path():
    """
    Ensure that all required directories exists
    """
    p = os.environ.get("SHORT_JOB_PATH", None)
    if p is None:
        p = os.path.join(os.path.expanduser('~'), ".short-job-queue")
    Path(os.path.join(p, "todo")).mkdir(parents=True, exist_ok=True)
    Path(os.path.join(p, "failure")).mkdir(parents=True, exist_ok=True)
    Path(os.path.join(p, "success")).mkdir(parents=True, exist_ok=True)
    return p


def each_todo_file():
    """
    Iterate over all pending todo files
    """
    p = ensure_config_path()
    todo_path = os.path.join(p, "todo")
    for filename in os.listdir(todo_path):
        f = os.path.join(todo_path, filename)
        if f.endswith('.json') and os.path.isfile(f):
            yield f


def add_job_to_queue(job):
    """
    Add a job to the queue
    """
    p = ensure_config_path()
    name = job.job_names()[0] + '.json'
    job.save_to_file(os.path.join(p, "todo", name))
